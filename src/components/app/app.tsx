// NPM
import React, { useState } from 'react'

// Project
import Nutrition from '@components/nutrition/nutrition'
import { IPeriod, INutritionData } from 'types'
import { Button } from '@material-ui/core'
import { addConsumedToState, removeConsumedFromState } from '@utils/state'

export interface IProps {

}
interface IState {
  nutritionData: INutritionData
}

const savedState = window.localStorage.getItem('state')

const emptyState = {
  nutritionData: {
    consumptions: []
  }
}

export const App = (props: IProps) => {
  const [loadWasAttempted, attemptLoad] = useState(false)
  const [state, setState] = useState<IState>(emptyState)

  if (!loadWasAttempted && state.nutritionData.consumptions.length === 0 && savedState) {
    attemptLoad(true)
    setState(JSON.parse(savedState))
  }
  window.localStorage.setItem('state', JSON.stringify(state))

  const addConsumed = (groupCode: string, period: IPeriod) =>
    addConsumedToState(state, setState, groupCode, period)
  const removeConsumed = (groupCode: string, period: IPeriod) =>
    removeConsumedFromState(state, setState, groupCode, period)

  return <>
    <h3 style={{ marginLeft: '8px'}}>Synergy Wellness Prototype</h3>
    <Nutrition
      nutritionData={state.nutritionData}
      addConsumed={addConsumed}
      removeConsumed={removeConsumed}
    />
    <Button
      style={{ backgroundColor: 'red', margin: '30px', float: 'right' }}
      onClick={() => setState(emptyState)}
    >
      Reset
    </Button>
  </>
}

export default App

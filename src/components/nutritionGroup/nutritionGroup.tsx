// NPM
import * as React from 'react'

// Project
import NutritionUnit, { IProps as INutritionUnitProps } from '@components/nutritionUnit/nutritionUnit'
import { IGroup, IPeriod } from 'types'

const transformGroupDataToUnitPropsArray = (groupData: IProps): INutritionUnitProps[] => {
  const propsArr: INutritionUnitProps[] = []
  const unitsDisplayed = groupData.allocated > groupData.consumed
    ? groupData.allocated
    : groupData.consumed + 1

  for (let i = 1; i <= unitsDisplayed; i++) {
    const isFilled = (i <= groupData.consumed)
    const isAddable = i === groupData.consumed + 1
    const isRemovable = i === groupData.consumed
    const isOverallocation = i > groupData.allocated
    propsArr.push({
      baseColor: groupData.group.baseColor,
      isAddable,
      isRemovable,
      isOverallocation,
      isFilled,
      onClick: isAddable
        ? groupData.addConsumed
        : isRemovable
          ? groupData.removeConsumed
          : () => { return }
      // handleRemoveConsumed: groupData.handleRemoveConsumed,
    })
  }
  return propsArr
}
export interface IProps {
  period: IPeriod
  group: IGroup
  consumed: number
  allocated: number
  addConsumed: () => void
  removeConsumed: () => void
}
export const UnitGroup = (props: IProps) => {
  const unitPropsArr = transformGroupDataToUnitPropsArray(props)
  const styles = {
    unitGroup: {
      display: 'flex',
      borderTop: '1px solid lightgrey',
      margin: '8px 0',
      paddingTop: '8px'
    },
    ola: {
      color: props.group.baseColor,
      fontFamily: 'arial',
      fontSize: '1.2rem',
      padding: '0 4px',
      width: '1.2rem',
      alignSelf: 'center'
    }
  }
  return <div className="unit-group" style={styles.unitGroup}>
    <div style={styles.ola}>{props.group.ola}</div>
    <div>
      {unitPropsArr.map((p, key) => <NutritionUnit key={key} {...p} />) }
    </div>
  </ div>
}

export default UnitGroup

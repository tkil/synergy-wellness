// NPM
import React, { useState } from 'react'

// Material
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import Typography from '@material-ui/core/Typography'

// Project
import { wellnessConfig } from '@config/wellness'
import NutritionGroup from '@components/nutritionGroup/nutritionGroup'
import { countConsumptions } from '@utils/misc'
import { IGroup, INutritionData, IPeriod } from 'types'

const useStyles = makeStyles({
  expansionPanelSummary: {
    padding: '4px 8px',
    '&.Mui-expanded': {
      minHeight: 'auto'
    },
    '& div.Mui-expanded': {
      margin: '4px'
    },
    '& div': {
      display: 'flex',
      'justify-content': 'space-between',
      margin: '4px'
    },
    minHeight: 'auto'
  },
  expansionPanelDetails: {
    padding: '4px'
  }
})

export interface IProps {
  period: IPeriod
  nutritionData: INutritionData
  addConsumed: (groupCode: string, period: IPeriod) => void
  removeConsumed: (groupCode: string, period: IPeriod) => void
}

export const NutritionPeriod = (props: IProps) => {
  const cals = wellnessConfig.groups.reduce((acc, group) =>
    acc
    + (countConsumptions(props.nutritionData.consumptions, props.period).get(group.groupCode) || 0)
    * group.unitCalories
  , 0)
  const classes = useStyles()
  return <ExpansionPanel>
    <ExpansionPanelSummary className={classes.expansionPanelSummary}>
      <div><Typography>{props.period.label}</Typography></div>
      <div><Typography>{cals}</Typography></div>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails className={classes.expansionPanelDetails}>
      <div style={{'width': '100%'}}>
      {
        wellnessConfig.groups.map((group: IGroup, key) =>
          <NutritionGroup
            key={key}
            period={props.period}
            group={group}
            addConsumed={() => props.addConsumed(group.groupCode, props.period)}
            removeConsumed={() => props.removeConsumed(group.groupCode, props.period)}
            allocated={4}
            consumed={countConsumptions(props.nutritionData.consumptions, props.period).get(group.groupCode) || 0}
          />
        )
      }
    </div>
    </ExpansionPanelDetails>
  </ExpansionPanel>

}

export default NutritionPeriod

// NPM
import * as React from 'react'

// Material
import Chip from '@material-ui/core/Chip'

// Project
import { colors, darken } from '@utils/colors'

const getUnitStyle = (props: IProps) => ({
  backgroundColor: props.isOverallocation
  ? props.isFilled
    ? darken(props.baseColor, 0.33)
    : colors.silver
  : props.isFilled
    ? props.baseColor
    : colors.clouds,
  borderColor: props.isOverallocation
    ? darken(props.baseColor, 0.33)
    : props.baseColor,
  borderWidth: '2px',
  borderStyle: 'solid',
  height: '20px',
  margin: '6px',
  width: '40px'
})
export interface IProps {
  baseColor: string
  onClick?: () => void,
  isAddable?: boolean,
  isFilled?: boolean,
  isOverallocation?: boolean,
  isRemovable?: boolean,
}
export const Unit = (props: IProps) => {
  const style = getUnitStyle(props)
  const isClickable = props.isAddable || props.isRemovable
  return <>
    <Chip
      clickable={isClickable}
      onClick={props.onClick}
      style={style}
    />
  </>
}

export default Unit

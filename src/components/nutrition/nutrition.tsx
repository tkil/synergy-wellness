// NPM
import React from 'react'

// Project
import { wellnessConfig } from '@config/wellness'
import NutritionGroup from '@components/nutritionGroup/nutritionGroup'
import NutritionPeriod from '@components/nutritionPeriod/nutritionPeriod'
import { countConsumptions, removeLastConsumedIndex } from '@utils/misc'
import { IGroup, INutritionData, IObject, IPeriod } from 'types'

export interface IProps {
  nutritionData: INutritionData
  addConsumed: (groupCode: string, period: IPeriod) => void
  removeConsumed: (groupCode: string, period: IPeriod) => void
}
export const Nutrition = (props: IProps) => {
  let totalCal = 0
  const consumed = countConsumptions(props.nutritionData.consumptions)
  return <>
    {
      wellnessConfig.periods.map((period, key) =>
        <NutritionPeriod
          key={key}
          nutritionData={props.nutritionData}
          addConsumed={props.addConsumed}
          removeConsumed={props.removeConsumed}
          period={period}
        />
      )
    }
    <section style={{ marginTop: '20px' }}>
      {
        wellnessConfig.groups.map((g, key) => {
          const alloc = wellnessConfig.dailyAllocations.find((a) => a.groupCode === g.groupCode)
          const min = alloc ? alloc.min : 0
          const max = alloc ? alloc.max : 0
          const count = consumed.get(g.groupCode) || 0
          const cal = count * g.unitCalories
          totalCal += cal
          return <div key={key}>{g.label} [{min}-{max}]: <b>{count}</b> - {cal}cal</div>
        })
      }

    </section>
    <br />
    <b>
      Total Calories: ~{totalCal}
    </b>
  </>
}

export default Nutrition


import { colors } from '@utils/colors'
import { IWellnessConfig } from 'types'

export const wellnessConfig: IWellnessConfig = {
  groups: [
    {
      groupCode: 'water',
      unitCalories: 0,
      label: 'Water',
      ola: 'W',
      symbol: '',
      baseColor: colors.belizeHole
    },
    {
      groupCode: 'vegetable',
      unitCalories: 50,
      label: 'Veggies (Lean)',
      ola: 'V',
      symbol: '',
      baseColor: colors.pixelatedGrass
    },
    {
      groupCode: 'protien',
      unitCalories: 75,
      label: 'Protien',
      ola: 'P',
      symbol: '',
      baseColor: colors.alizarin
    },
    {
      groupCode: 'grains',
      unitCalories: 75,
      label: 'Grains',
      ola: 'G',
      symbol: '',
      baseColor: colors.sunFlower
    },
    {
      groupCode: 'fruit',
      unitCalories: 100,
      label: 'Fruits',
      ola: 'F',
      symbol: '',
      baseColor: colors.turquoise
    },
    {
      groupCode: 'lipid',
      unitCalories: 100,
      label: 'Lipids',
      ola: 'L',
      symbol: '',
      baseColor: colors.pumpkin
    }
  ],
  periods: [
    {
      periodCode: 'breakfast',
      label: 'Breakfast',
      beginHour: 6,
      endHour: 9
    },
    {
      periodCode: 'midMorning',
      label: 'Mid-morning',
      beginHour: 9,
      endHour: 11
    },
    {
      periodCode: 'lunch',
      label: 'Lunch',
      beginHour: 11,
      endHour: 14
    },
    {
      periodCode: 'midAfternoon',
      label: 'Mid-afternoon',
      beginHour: 14,
      endHour: 17
    },
    {
      periodCode: 'dinner',
      label: 'Dinner',
      beginHour: 17,
      endHour: 20
    },
    {
      periodCode: 'evening',
      label: 'Evening',
      beginHour: 20,
      endHour: 24
    }
  ],
  dailyAllocations: [
    {
      groupCode: 'water',
      min: 16,
      max: 20
    },
    {
      groupCode: 'vegetable',
      min: 8,
      max: 12
    },
    {
      groupCode: 'protien',
      min: 6,
      max: 8
    },
    {
      groupCode: 'starch',
      min: 4,
      max: 6
    },
    {
      groupCode: 'fruit',
      min: 0,
      max: 3
    },
    {
      groupCode: 'lipid',
      min: 1,
      max: 3
    }
  ],
  periodAllocations: [
    {
      groupCode: 'vegetable',
      periodCode: 'breakfast',
      max: 2,
      min: 4
    }
  ],
  dailyCalories: {
    minCalories: 0,
    maxCalories: 0
  },
  periodCalories: [
    {
      periodCode: '',
      minCalories: 0,
      maxCalories: 0
    }
  ]
}

// {
//   name: 'breakfast'
//   minCalories: 300,
//   maxCalories: 400,
//   beginHour: 6,
//   endHour: 9,
// },
// {
//   name: 'midMorning'
//   minCalories: 0,
//   maxCalories: 0,
//   beginHour: 9,
//   endHour: 11,
// },
// {
//   name: 'lunch'
//   minCalories: 500,
//   maxCalories: 700,
//   beginHour: 11,
//   endHour: 14,
// },
// {
//   name: 'midAfternoon'
//   minCalories: 0,
//   maxCalories: 200,
//   beginHour: 14,
//   endHour: 17,
// },
// {
//   name: 'dinner'
//   minCalories: 500,
//   maxCalories: 700,
//   beginHour: 17,
//   endHour: 20,
// },
// {
//   name: 'evening'
//   minCalories: 0,
//   maxCalories: 0,
//   beginHour: 20,
//   endHour: 22,
// },

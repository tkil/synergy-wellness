
import { removeLastConsumedIndex } from '@utils/misc'
import { IObject, IPeriod } from 'types'

export const addConsumedToState = (state: IObject, setState: any, groupCode: string, period: IPeriod) =>
  setState({
    ...state,
    nutritionData: {
      ...state.nutritionData,
      consumptions: [
        ...state.nutritionData.consumptions,
        {
          amount: 1,
          groupCode,
          epochTime: new Date((new Date()).toDateString()).getTime() + period.beginHour * 60 * 60 * 1000
        }
      ]
    }
  })

export const removeConsumedFromState = (state: IObject, setState: any, groupCode: string, period: IPeriod) => {
  const consumptions = removeLastConsumedIndex(state.nutritionData.consumptions, groupCode, period)
  setState({
    ...state,
    nutritionData: {
      ...state.nutritionData,
      consumptions
    }
  })
}
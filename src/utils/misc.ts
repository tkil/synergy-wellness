import { IConsumption, IPeriod } from 'types'

export const isConsumptionInPeriod = (consumption: IConsumption, period: IPeriod) =>
  new Date(consumption.epochTime).getHours() >= period.beginHour
      && new Date(consumption.epochTime).getHours() < period.endHour

export const countConsumptions =
  (consumptions: IConsumption[], period?: IPeriod): Map<string, number> =>
  consumptions.reduce((acc, c) => {
    if (!period || isConsumptionInPeriod(c, period)) {
      const prevCount = acc.get(c.groupCode) || 0
      acc.set(c.groupCode, prevCount + c.amount)
    }
    return acc
  }, new Map<string, number>())

export const removeLastConsumedIndex = (consumptions: IConsumption[], groupCode: string, period: IPeriod) => {
  const index = consumptions.slice().reverse()
  .findIndex((c) => (c.groupCode === groupCode) && isConsumptionInPeriod(c, period))
  if (index > -1) {
    const count = consumptions.length - 1
    const finalIndex = index >= 0 ? count - index : index
    const result = [ ...consumptions]
    result.splice(finalIndex, 1)
    return result
  }
  return consumptions
}

// NPM
import React from 'react'
import { render } from 'react-dom'

// Project
import App from '@components/app/app'

export default () => {
  render(
    <App />,
    document.getElementById('app')
  )
}

export interface IGroup {
  groupCode: string
  label: string
  ola: string
  symbol: string
  baseColor: string
  unitCalories: number
}

export interface IConsumption {
  groupCode: string
  epochTime: number
  amount: number
}

export interface IObject {
  [key: string]: any
}

export interface IPeriod {
  periodCode: string
  label: string
  beginHour: number
  endHour: number
}

export interface IAllocation {
  groupCode: string,
  min: number
  max: number
}

export interface IPeriodAllocation extends IAllocation {
  periodCode: string,
}

export interface ICalories {
  minCalories: number
  maxCalories: number
}

export interface IPeriodCalories extends ICalories {
  periodCode: string
}

export interface IWellnessConfig {
  groups: IGroup[]
  periods: IPeriod[]
  dailyAllocations: IAllocation[]
  periodAllocations: IPeriodAllocation[]
  dailyCalories: ICalories
  periodCalories: IPeriodCalories[]
}

export interface INutritionData {
  consumptions: IConsumption[]
}


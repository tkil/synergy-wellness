// Tools
const path = require('path');

// Plugins
const HtmlWebpackPlugin = require('html-webpack-plugin')
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

// Pathing
const dir = __dirname
const srcDir = path.join(dir, 'src')
const dist = path.join(dir, 'dist')


module.exports = {
  entry: path.join(srcDir, 'index.ts'),
  devServer: {
    open: true,
    openPage: 'index.html'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
    plugins: [new TsconfigPathsPlugin({ configFile: path.join(dir, 'tsconfig.json') })]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(srcDir, 'index.html'),
    }),
  ],
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, 'dist'),
  },
};